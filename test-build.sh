#!/usr/bin/env sh

browser=${1:-firefox}
export VERSION=${VERSION:-0.0.1}

rm -rf dist
mkdir -p dist
cp -R src/common/* dist
cp -R src/firefox/* dist
envsubst < dist/manifest.json.template > dist/manifest.json
rm dist/manifest.json.template

pushd dist/
zip -r "../$browser.zip" .
popd
