try {
  browser
} catch (e) {
  browser = chrome
}

function chromeReadClipboard() {
  // Create hidden input to receive text
  const el = document.createElement('textarea')
  el.value = 'before paste'
  document.body.append(el)

  // Paste from clipboard into input
  el.select()
  const success = document.execCommand('paste')

  // The contents of the clipboard
  const text = el.value
  el.remove()

  // Resolve with the contents of the clipboard
  return success
    ? Promise.resolve(text)
    : Promise.reject(new Error('Unable to read from clipboard'))
}

function readClipboard () {
  return navigator.clipboard.readText()
    .catch(e => {
      console.log(e)
      return chromeReadClipboard()
    })
}

async function pasteValues (tab) {
  var text = await readClipboard()

  console.log(text)

  text
    .split("&")
    .map(keyValue => {
      const keyValueParts = keyValue
        .split("=")
        .map(v => v.trim())

      let value = keyValueParts[1]

      try {
        value = decodeURIComponent(value)
      } catch (e) {
      }

      const key = keyValueParts[0]

      if (key[0] === '#') {
        const id = key.substring(1)

        return { id, value }
      }

      if (key[0] === '(' && key[key.length - 1] === ')') {
        let query = key.substring(1, key.length - 1)

        try {
          query = decodeURIComponent(query)
        } catch (e) {
        }

        return { query, value }
      }

      return { name: key, value }
    })
    .forEach(item => {
      var elementsGetter

      if (item.name) {
        elementsGetter = `document.getElementsByName("${item.name}")`
      } else if (item.id) {
        elementsGetter = `[document.getElementById("${item.id}")]`
      } else if (item.query) {
        elementsGetter = `document.querySelectorAll("${item.query}")`
      } else {
        throw new Error("Unexpected case for pasting data.")
      }

      browser.tabs.executeScript(
        tab.id,
        { code: `
          ${elementsGetter}.forEach(input => {
            if (["radio", "checkbox"].includes(input.type)) {
              input.checked = ${item.value === "true"}
              input.dispatchEvent(new Event("change", { cancellable: true, bubbles: true }))

              return
            }

            input.value = "${item.value}"
            input.dispatchEvent(new Event("input", { cancellable: true, bubbles: true }))
          })
          `
        }
      )
    })
}

browser.tabs.onUpdated.addListener((id, changeInfo, tab) => {
  browser.pageAction.show(tab.id);
});

browser.pageAction.onClicked.addListener(pasteValues)
