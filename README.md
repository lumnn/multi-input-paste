# Multi Input Paste

> Web Extension that allows pasting into multiple input fields. A great choice
> to automate copying data between systems

![promo](promo.png)

## Installation

[![Firefox](https://ffp4g1ylyit3jdyti1hqcvtb-wpengine.netdna-ssl.com/addons/files/2015/11/get-the-addon.png)](https://addons.mozilla.org/en-US/firefox/addon/multi-input-paste/)

[![Chrome](https://developer.chrome.com/webstore/images/ChromeWebStore_Badge_v2_206x58.png)](https://chrome.google.com/webstore/detail/multi-input-paste/jbpipkhgfpdikckgllcfgnmobojcbfld)

## Demo page to test

[Open Demo Page](https://lumnn.gitlab.io/multi-input-paste)

## Issues / Suggestions

Please report all issues and suggestions on the "Issues" page on Gitlab repository
